<% from common import htmlname, ttip, htmlttip, boxedletter %>

<%inherit file="common.mako"/>
<%namespace name="helpers" file="helpers.mako"/>

<%block name="title">${builds[0] if builds else ""} CI Results</%block>

<table>
  <tr>
    <th></th>
    % for build in builds:
      % if not loop.index or builds[loop.index-1] != build:
      % if build in desc:
        <th class="g" colspan=${builds.count(build)}><a href="${path}${build}/">${build}<br>${desc[build]}</a></th>
      % else:
        <th class="g" colspan=${builds.count(build)}><a href="${path}${build}/">${build}</a></th>
      % endif
      <th class="s"></th>
      % endif
    % endfor
  </tr>
  <tr>
    <th></th>
    % for host in hosts:
     %  if loop.index and builds[loop.index-1] != builds[loop.index]:
      <th class="s"></th>
     % endif
      <th><span class="v"><a href="${path}${builds[loop.index]}/${host}/">${host}</a></th>
    % endfor
  </tr>

  % for test in tests:
    <tr id="${test}">
      <td class="h"><a href="${path}${htmlname(test)}">${test}</a></td>
      % for host in hosts:
        % if loop.index and builds[loop.index-1] != builds[loop.index]:
      <td class="s"></td>
        % endif
        <%
          try:
              node = jsons[(builds[loop.index], host)]['tests'][test]
              if 'hostname' in node: realhost = node['hostname']
              else: realhost = hosts[loop.index]
              res,tooltip = ttip(node)
              letter = boxedletter(node)
              header = builds[loop.index]+" / "+realhost
              link = path+builds[loop.index]+"/"+realhost+"/"+htmlname(test)
          except:
              res,tooltip = ('notrun', "")
              letter = '&nbsp;'
              header = ""
              link = ""

          tooltip=htmlttip(tooltip)
        %>
      ${helpers.result_cell(res, link, letter, test, builds[loop.index], tooltip)}
      % endfor
    </tr>
  % endfor
</table>
